import hashlib
import hmac

for b in 'CL':
    for c in 'AEHJKU':
        for d in 'DST':
            for i in range(0, 100000):
                titleid = 'B'+b+c+d+str(i).zfill(5)+'_00'
                digester = hmac.new(bytes.fromhex('F5DE66D2680E255B2DF79E74F890EBF349262F618BCAE2A9ACCDEE5156CE8DF2CDF2D48C71173CDC2594465B87405D197CF1AED3B7E9671EEB56CA6753C2E6B0'), bytes(titleid, 'UTF-8'), hashlib.sha1)
                print(titleid, digester.hexdigest().upper())